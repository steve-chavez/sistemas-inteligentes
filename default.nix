with import <nixpkgs>{};
stdenv.mkDerivation {
  name = "SistemasInteligentes";
  buildInputs = with python36Packages; [
    jupyter
    matplotlib
    numpy
    seaborn
    scikitlearn
    tensorflow 
    Keras
    pydot
  ];
}
