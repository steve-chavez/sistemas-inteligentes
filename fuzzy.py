from functools import partial

# Distribuciones estandar de conjuntos difusos

def triangular(a, b, c, x):
    # De https://www.mathworks.com/help/fuzzy/trimf.html
    if x <= a:
        return 0.0
    elif x <= b:
        return (x - a) / (b - a)
    elif x <= c:
        return (c - x) / (c - b)
    else:
        return 0.0

def trapezoidal(a, b, c, d, x):
    # De https://www.mathworks.com/help/fuzzy/trapmf.html
    if x <= a:
        return 0.0
    elif x <= b:
        return (x - a) / (b - a)
    elif x <= c:
        return 1.0
    elif x <= d:
        return (d - x) / (d - c)
    else:
        return 0.0

def forma_de_z(a, b, x):
    # Basado en https://www.mathworks.com/help/fuzzy/zmf.html
    if x <= a:
        return 1.0
    elif x <= b:
        return (b - x)/(b - a)
    else:
        return 0.0

def forma_de_s(a, b, x):
    # Basado en https://www.mathworks.com/help/fuzzy/smf.html
    if x <= a:
        return 0.0
    elif x <= b:
        return (x - a)/(b - a)
    else:
        return 1.0

# Operaciones booleanas en conjuntos difusos

def y(x, y):
    return min(x, y)

def o(x, y):
    return max(x, y)

def entonces(val, f, x):
    return val * f(x)

def reglas(lfs, x):
    return sum([f(x) for f in lfs])

def centroide(dominio, funcion_membresia):
    x = list(map(funcion_membresia, dominio))
    return sum([a * b for (a, b) in zip(dominio, x)])/sum(x)

# Definiendo los conjuntos difusos del problema

# Precio

def precio_barato(x):
    return forma_de_z(1000.0, 1500.0, x)

def precio_accesible(x):
    return triangular(1000.0, 1500.0, 2000.0, x)

def precio_caro(x):
    return forma_de_s(1500.0, 2000.0, x)

# Ingreso

def ingreso_bajo(x):
    return forma_de_z(200.0, 500.0, x)

def ingreso_medio(x):
    return triangular(300.0, 600.0, 800.0, x)

def ingreso_alto(x):
    return forma_de_s(600.0, 900.0, x)

# Ventas

def ventas_bajas(x):
    return forma_de_z(10_000.0, 30_000.0, x)

def ventas_normales(x):
    return triangular(10_000.0, 30_000.0, 50_000.0, x)

def ventas_altas(x):
    return forma_de_s(30_000.0, 50_000.0, x)

# Resolucion del problema
precio = 1400
ingreso = 450

# Mostrar grados de pertenencia

print('\nPara un precio de {} y un ingreso de {}'.format(precio, ingreso))

print('\nEl grado de membresia para precio barato es: {}'.format(precio_barato(precio)))
print('El grado de membresia para precio accesible es: {}'.format(precio_accesible(precio)))
print('El grado de membresia para precio caro es: {}'.format(precio_caro(precio)))

print('\nEl grado de membresia para ingreso bajo es: {}'.format(ingreso_bajo(ingreso)))
print('El grado de membresia para ingreso medio es: {}'.format(ingreso_medio(ingreso)))
print('El grado de membresia para ingreso alto es: {}'.format(ingreso_alto(ingreso)))

print('\nEl centroide para ventas bajas es: {}'.format(centroide(range(10_000, 30_000), ventas_bajas)))
print('El centroide para ventas normales es: {}'.format(centroide(range(10_000, 50_000), ventas_normales)))
print('El centroide para ventas altas es: {}'.format(centroide(range(10_000, 50_000), ventas_altas)))

def ventas_proyectadas(precio, ingreso):
    return centroide(range(10_000, 50_000), partial(reglas, [
        partial(entonces, y(precio_barato(precio), ingreso_bajo(ingreso)), ventas_bajas),
        partial(entonces, y(precio_barato(precio), ingreso_medio(ingreso)), ventas_normales),
        partial(entonces, y(precio_accesible(precio), ingreso_bajo(ingreso)), ventas_bajas),
        partial(entonces, y(precio_accesible(precio), ingreso_medio(ingreso)), ventas_bajas),
        partial(entonces, y(precio_accesible(precio), ingreso_alto(ingreso)), ventas_normales),
        partial(entonces, y(precio_caro(precio), ingreso_bajo(ingreso)), ventas_bajas),
        partial(entonces, y(precio_caro(precio), ingreso_medio(ingreso)), ventas_bajas),
        partial(entonces, y(precio_caro(precio), ingreso_alto(ingreso)), ventas_normales),
    ]))

print('\nLas ventas proyectadas son: {}'.format(ventas_proyectadas(precio, ingreso)))
